# Docker Deploy: Grafana 

This repo contains a demo setup for the [django-prometheus](https://github.com/korfuri/django-prometheus) plugin,
which exposes Django usage metrics as a Prometheus end point. See their README on how to use it.

## Spin up

To build and spin up the demo environment:

```
docker-compose build
docker-compose up
```

## Look around

Once the stack has booted up, you can visit:

* http://localhost:8000/polls to get to the entry point for the API of the demo app.
* http://localhost:8000/metrics to get the metrics exposed to Prometheus.
* http://localhost:3000 to go to Grafana, where you can view the "Django Prometheus" dashboard

You will see that the django-prometheus plugin will expose general statistics like:
* Backend database performance,
* HTTP response times,
* CRUD operations on models that have been extended with the ```ExportModelOperationsMixin``` class.

Example metric exposed by this plugin (for a full list see ```example-exposed-metrics.txt```):

```
# HELP django_model_inserts_total Number of insert operations by model.
# TYPE django_model_inserts_total counter
django_model_inserts_total{model="question"} 0.0
django_model_inserts_total{model="choice"} 0.0
```

## Notes

The plugin's documentation describes well how Django is to be extended to provide the above
metrics, and this repo allows you to play with such a setup.

The setup also includes a configuration for Prometheus. The provided dashboard in Grafana relies on 
```prometheus/django.rules``` to be active, to evaluate its formulas faster when asked.

