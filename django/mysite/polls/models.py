from django.db import models
from django_prometheus.models import ExportModelOperationsMixin


class Question(ExportModelOperationsMixin('question'), models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")


class Choice(ExportModelOperationsMixin('choice'), models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
